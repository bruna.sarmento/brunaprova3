const prompt = require('prompt');

const integer2 = async  () =>{    
    prompt.start() 

    const integer = await prompt.get(['totalNumberOfOxen']);

    let totalNumberOfOxen = integer.totalNumberOfOxen;
    let stopCondition = 0
    let greaterWeight= 0
    let lighterWeight = 100000 
    

    do{
        const im = await prompt.get(['id', 'weights','name' ]);
      
        stopCondition += 1
	    
        if (im.weight > greaterWeight){
		    greaterWeight = im.weight
		    id2 = im.id
		    name2 = im.name
        }
        else if (im.weight < lighterWeight){
            let lighterWeight = im.weight
		    id1 = im.id
		    name1 = im.name
        }       	
    } while(stopCondition < totalNumberOfOxen);

    console.log(`Gordo:  ${id2} | Peso: ${greaterWeight} | Nome: ${name2}`)

    console.log(`Magro: ${id1} | Peso: ${lighterWeight} | Nome: ${name1}`)
}
integer2();